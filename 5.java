public class Program {
	public static String flipEndChars(String s) {
		if (s.length() < 2) {return "Incompatible.";}
		int last = s.length() - 1;
		char start = s.charAt(0), end = s.charAt(last);
		if  (start == end) {return "Two's a pair.";}
		return end + s.substring(1, last) + start;
	}
}
