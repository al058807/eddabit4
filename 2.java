public class Challenge {
  public static int warOfNumbers(int[]numbers){
		int even = 0; 
		int odd = 0;
		for(int num : numbers) {
			if(num % 2 == 0) {
				even += num;
			} else {
				odd += num;
			}
		}
		return Math.max(even,odd) - Math.min(even,odd);
  }
}
